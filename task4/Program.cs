﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = 0, width= 0;
            Console.WriteLine("Please enter a Length:");
            int.TryParse(Console.ReadLine(), out length);
            Console.WriteLine("Please enter a Width:");
            int.TryParse(Console.ReadLine(), out width);

            for (int i = 1; i <= length; i++)
            {
                for (int j= 1; j <= width; j++)
                {
                    if (i == 1 || i == length || j == 1 || j == width)
                    {
                        Console.Write("#");
                    }
                    else if ((i==3 || i==length-2) && (j!= 2 && j!= width-1))
                    {
                        Console.Write("#");
                    }
                    else if ((j == 3 || j == width - 2) && (i != 2 && i != length - 1))
                    {
                        Console.Write("#");
                    }
                    else Console.Write(" ");



                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
