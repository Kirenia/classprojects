﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_Kirenia
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "";
            Console.WriteLine("Please enter your name");
            name = Console.ReadLine();
            char initial = name[0];
            char last = name[name.Length - 1];
            Console.WriteLine($"Hello,{name} your name is {name.Length} long, start with {initial} and ends with {last}");
            Console.WriteLine("Enter your last name");
            string lastname = Console.ReadLine();
            Console.WriteLine($"Your full name is {name} {lastname}");
            Console.ReadLine();
        }
       
    }
}
