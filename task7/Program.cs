﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> fibonacciList = new List<int>();
            fibonacciList.Add(1);
            fibonacciList.Add(2);
            int sum = 0;
            int previous = fibonacciList[0];
            while (fibonacciList[fibonacciList.Count -1] < 4000000)
            {
                int next = fibonacciList[fibonacciList.Count - 1] + previous;
               
                if (next > 4000000)
                {
                
                    break;
                }
                else
                {
                    fibonacciList.Add(next);
                }
                previous = fibonacciList[fibonacciList.Count - 2];

            }

            for (int i = 0; i < fibonacciList.Count; i++)
            {
                if (fibonacciList[i]% 2==0 )
                {
                    sum += fibonacciList[i];
                }
               

            }
            Console.WriteLine($"The sum of the even-valued terms in the fibonacci list er: {sum}");
          
            Console.ReadLine();
        }

    }
}
